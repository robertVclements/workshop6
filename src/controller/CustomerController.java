package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */

public class CustomerController implements Initializable {

    Agent loginUser;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnEdit;

    @FXML
    private TableView<Customer> tvCustomer;

    @FXML
    private TableColumn<Customer, Integer> clCustomerId;

    @FXML
    private TableColumn<Customer, String> clCustFirstName;

    @FXML
    private TableColumn<Customer, String> clCustLastName;

    @FXML
    private TableColumn<Customer, String> clCustAddress;

    @FXML
    private TableColumn<Customer, String> clCustCity;

    @FXML
    private TableColumn<Customer, String> clCustProv;

    @FXML
    private TableColumn<Customer, String> clCustPostal;

    @FXML
    private TableColumn<Customer, String> clCustCountry;

    @FXML
    private TableColumn<Customer, String> clCustHomePhone;

    @FXML
    private TableColumn<Customer, String> clCustBusPhone;

    @FXML
    private TableColumn<Customer, String> clCustEmail;

    @FXML
    private TableColumn<Customer, Agent> clAgent;

    @FXML
    void addNewCustomer(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("../fxml/modifyCustomer.fxml"));
        Parent addPage = loader.load();

        Scene sceneAdding = new Scene(addPage);
        sceneAdding.getStylesheets().add("/styleSheet/addData.css");

        Stage windowAdding = new Stage();
        windowAdding.setTitle("Add New Customer");
        windowAdding.setScene(sceneAdding);

        ModifyCustomerController controller = loader.getController();
        controller.setDefaultOfAdding(loginUser);

        windowAdding.showAndWait(); // need wait util the windowAdding closed
        tvCustomer.setItems(CustomerDB.getAllCustomers()); //refresh the tvCustomer

    }

    @FXML
    void editCustomer(ActionEvent event) throws IOException {

        //get selected customer data
        Customer selectedCust = tvCustomer.getSelectionModel().getSelectedItem();

        if ((selectedCust == null ))
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            //alert.setTitle("Error Message");
            //alert.setHeaderText("Results:");
            alert.setContentText("Please select a customer for editing.");

            alert.showAndWait();
        }
        else
        {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../fxml/modifyCustomer.fxml"));
            Parent editPage = loader.load();

            Scene sceneEditing = new Scene(editPage);
            sceneEditing.getStylesheets().add("/styleSheet/addData.css");

            Stage windowEditing = new Stage();
            windowEditing.setTitle("Edit the Customer");
            windowEditing.setScene(sceneEditing);

            ModifyCustomerController controller = loader.getController();
            controller.setDefaultOfEditing(selectedCust);

            windowEditing.showAndWait();// need wait util the windowAdding closed
            tvCustomer.setItems(CustomerDB.getAllCustomers()); //refresh the tvCustomer

        }

    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //clears the TableView preparing for loading latest data
        tvCustomer.getColumns().clear();

        //loads the data from database to the TableView
        loadCustomerTable();

    }

    //The method is to create customer columns in the table view, get and shows the data from the database
    public void loadCustomerTable(){
        //create columns
        clCustomerId.setCellValueFactory(new PropertyValueFactory<>("customerId"));

        clCustFirstName.setCellValueFactory(new PropertyValueFactory<>("custFirstName"));

        clCustLastName.setCellValueFactory(new PropertyValueFactory<>("custLastName"));

        clCustAddress.setCellValueFactory(new PropertyValueFactory<>("custAddress"));

        clCustCity.setCellValueFactory(new PropertyValueFactory<>("custCity"));

        clCustProv.setCellValueFactory(new PropertyValueFactory<>("custProv"));

        clCustPostal.setCellValueFactory(new PropertyValueFactory<>("custPostal"));

        clCustCountry.setCellValueFactory(new PropertyValueFactory<>("custCountry"));

        clCustHomePhone.setCellValueFactory(new PropertyValueFactory<>("custHomePhone"));

        clCustBusPhone.setCellValueFactory(new PropertyValueFactory<>("custBusPhone"));

        clCustEmail.setCellValueFactory(new PropertyValueFactory<>("custEmail"));

        clAgent.setCellValueFactory(new PropertyValueFactory<>("agent"));

        //load data to tvBooking
        tvCustomer.setItems(CustomerDB.getAllCustomers());

        //add columns
        tvCustomer.getColumns().addAll(clCustomerId, clCustFirstName, clCustLastName, clCustAddress,
                clCustCity, clCustProv, clCustPostal, clCustCountry, clCustHomePhone, clCustBusPhone,
                clCustEmail, clAgent);
    }

    public void initData(Agent user) {
        loginUser = user;
    }


}
