package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sample.*;

import java.awt.print.Book;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;


/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */

public class ModifyBookingController implements Initializable {

    //declare of the model of the modifying the booking table
    private enum Models { add, edit};
    private Models model;
    private Booking origBooking,newBooking;
    private Customer cust;
    private TripType tType;
    private TravelPackage tPackage;
    //gets current date time with Date()
    private LocalDate currentDate = LocalDate.now();
    ObservableList<String> status = FXCollections.observableArrayList("Open","Confirmed","Canceled","Closed");

    @FXML
    private AnchorPane anchorPan;

    @FXML
    private Label lblBookingDate;

    @FXML
    private TextField tfBookingNo;

    @FXML
    private TextField tfTravelerCount;

    @FXML
    private ComboBox<Customer> cmbCustomer;

    @FXML
    private ComboBox<TripType> cmbTripType;

    @FXML
    private ComboBox<TravelPackage> cmbTravelPackage;

    @FXML
    private ComboBox<String> cmbBookingStatus;

    @FXML
    private Button btnAction;

    @FXML
    private Button btnReset;

    @FXML
    private Label lblMessage;

    @FXML
    private Label lblBookingStatus;

    @FXML
    private Label lblStarSign;

    @FXML
    void reset(ActionEvent event) {

    }

    @FXML
    void action(ActionEvent event) {
        if (model == Models.valueOf("add"))
        {
            lblMessage.setText(null);
            cust = cmbCustomer.getSelectionModel().getSelectedItem();
            tType = cmbTripType.getSelectionModel().getSelectedItem();
            cmbTravelPackage.getSelectionModel().getSelectedItem();

            if (!(tfBookingNo.getText().isEmpty())
                    && !(tfTravelerCount.getText().isEmpty())
                    && (cmbCustomer.getValue() != null)
                    && Validation.isPositiveNumber(tfTravelerCount.getText())) {

                //create a new booking by passing the inputs, and assign a fake bookingId and assign the bookingStatus open
                Booking b = new Booking(0, currentDate, tfBookingNo.getText(), Integer.parseInt(tfTravelerCount.getText()),
                        cust.getCustomerId(), tType, tPackage, "Open");

                //insert the new booking into the database and check if successes
                if (BookingDB.addBooking(b)) {

                    // get a handle to the stage
                    Stage stage = (Stage) btnAction.getScene().getWindow();
                    // close the stage
                    stage.close();
                } else
                    lblMessage.setText("Failed to add the new booking, please try again.");
            } else
                lblMessage.setText("Please fill in all the fields with correct format value.");
        }
        else if ( model == Models.valueOf("edit"))
        {
            lblMessage.setText(null);
            cust = cmbCustomer.getSelectionModel().getSelectedItem();
            tType = cmbTripType.getSelectionModel().getSelectedItem();
            tPackage = cmbTravelPackage.getSelectionModel().getSelectedItem();
            String pkgStatus = cmbBookingStatus.getValue();

            if (!(tfBookingNo.getText().isEmpty())
                    && !(tfTravelerCount.getText().isEmpty())
                    && (cmbCustomer.getValue() != null)
                    && Validation.isPositiveNumber(tfTravelerCount.getText())
                    && (cmbTravelPackage.getValue() != null )) {

                newBooking = new Booking(origBooking.getBookingId(), origBooking.getBookingDate(), tfBookingNo.getText(), Integer.parseInt(tfTravelerCount.getText()),
                                            cust.getCustomerId(), tType, tPackage, pkgStatus); //bookingId and bookingDate are not editable

                if (BookingDB.updateBooking(origBooking, newBooking)) {
                    // get a handle to the stage
                    Stage stage = (Stage) btnAction.getScene().getWindow();
                    // close the stage
                    stage.close();
                } else
                    lblMessage.setText("Failed to edit the booking, please try again.");
            } else
                lblMessage.setText("Please fill in all the fields with correct format value.");
        }

    }


    /*@FXML
    void cancel(ActionEvent event) {

        // get a handle to the stage
        Stage stage = (Stage) btnSave.getScene().getWindow();
        // close the stage
        stage.close();
    }*/


    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }

    public void setDefaultOfAdding(){

        model = Models.add;
        //sets current date as default value of booking date, and it could not be edited
        lblBookingDate.setText(currentDate.toString());

        //get all customers from the database to the combo box
        ObservableList<Customer> custs = CustomerDB.getAllCustomers();
        cmbCustomer.getItems().addAll(custs);

        //get all tripTypes from the database to the combo box
        ObservableList<TripType> tripTypes = TripTypeDB.getTripTypes();
        cmbTripType.getItems().addAll(tripTypes);
        //sets the default value
        cmbTripType.getSelectionModel().selectFirst();

        //get all tripTypes from the database to the combo box
        ObservableList<TravelPackage> travelPkgs = TravelPackageDB.getAcitveTravelPackages();
        cmbTravelPackage.getItems().addAll(travelPkgs);
        //sets the default value
        cmbTravelPackage.getSelectionModel().selectFirst();

        //there is no need to display status for new adding, since it will automatically be assigned as open
        lblBookingStatus.setVisible(false);
        lblStarSign.setVisible(false);
        cmbBookingStatus.setVisible(false);

    }

    //get the booking data which is needed to be edited
    public void setDefaultOfEditing(Booking b) {

        model = Models.edit;
        origBooking = b;

        //display the selected booking data for editing and provide the options for the combo boxes
        lblBookingDate.setText(origBooking.getBookingDate().toString());
        tfBookingNo.setText(origBooking.getBookingNo());

        tfTravelerCount.setText(Integer.toString(origBooking.getTravelerCount()));

        ObservableList<Customer> custs = CustomerDB.getAllCustomers();
        cmbCustomer.getItems().addAll(custs);
        cmbCustomer.setValue(CustomerDB.getCustomerById(origBooking.getCustomerId()));
        cmbCustomer.setVisibleRowCount(5);


        ObservableList<TripType> tripTypes = TripTypeDB.getTripTypes();
        cmbTripType.getItems().addAll(tripTypes);
        cmbTripType.setValue(origBooking.getTripType());

        ObservableList<TravelPackage> pkgs = TravelPackageDB.getAcitveTravelPackages();
        cmbTravelPackage.getItems().addAll(pkgs);
        cmbTravelPackage.setValue(origBooking.getTravelPackage());

        lblBookingStatus.setVisible(true);
        lblStarSign.setVisible(true);
        cmbBookingStatus.setVisible(true);
        cmbBookingStatus.getItems().addAll(status);
        cmbBookingStatus.setValue(origBooking.getBookingStatus());

    }

}
