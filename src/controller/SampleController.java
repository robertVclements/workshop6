package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import sample.Agent;

import java.net.URL;
import java.io.IOException;
import java.util.ResourceBundle;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */

public class SampleController implements Initializable {

    private Agent loginUser;

    @FXML
    private Button btnUser;

    @FXML
    private Button btnCustomer;

    @FXML
    private Button btnPackage;

    @FXML
    private Button btnBooking;

    @FXML
    private Button btnProduct;

    @FXML
    private Button btnSignOut;

    @FXML
    private AnchorPane anchorTableView;

    /**
     * This method passes an user data to the local variable loginUser which can be used for the user profile.
     * It can be called when user clicks login button on login page, and pass the user data to loginUser.
     * @parame user
     */

    public void userInfo(Agent user) {
        loginUser = user;
        btnUser.setText(loginUser.getAgtFirstName() + " " + loginUser.getAgtLastName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        // Booking button clicked
        btnBooking.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    AnchorPane pane = FXMLLoader.load(getClass().getResource("../fxml/bookingTable.fxml"));
                    anchorTableView.getChildren().setAll(pane);
                    pane.prefWidthProperty().bind(anchorTableView.widthProperty());
                    pane.prefHeightProperty().bind(anchorTableView.heightProperty());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        // Customer button clicked
        btnCustomer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("../fxml/customerTable.fxml"));
                    AnchorPane pane = loader.load();
                    anchorTableView.getChildren().setAll(pane);
                    pane.prefWidthProperty().bind(anchorTableView.widthProperty());
                    pane.prefHeightProperty().bind(anchorTableView.heightProperty());

                    CustomerController controller = loader.getController();
                    controller.initData(loginUser);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        // Product button clicked
        btnProduct.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    AnchorPane pane = FXMLLoader.load(getClass().getResource("../fxml/productTable.fxml"));
                    anchorTableView.getChildren().setAll(pane);
                    pane.prefWidthProperty().bind(anchorTableView.widthProperty());
                    pane.prefHeightProperty().bind(anchorTableView.heightProperty());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        // Package button clicked
        btnPackage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    AnchorPane pane = FXMLLoader.load(getClass().getResource("../fxml/packageTable.fxml"));
                    anchorTableView.getChildren().setAll(pane);
                    pane.prefWidthProperty().bind(anchorTableView.widthProperty());
                    pane.prefHeightProperty().bind(anchorTableView.heightProperty());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        // Sign out button clicked
        btnSignOut.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.exit();
            }
        });

        // User button clicked
        btnUser.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("../fxml/userProfile.fxml"));
                    AnchorPane pane = loader.load();
                    anchorTableView.getChildren().setAll(pane);
                    pane.prefWidthProperty().bind(anchorTableView.widthProperty());
                    pane.prefHeightProperty().bind(anchorTableView.heightProperty());

                    UserProfileController controller = loader.getController();
                    controller.initData(loginUser);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}