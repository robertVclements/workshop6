package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import sample.Product;
import sample.ProductDB;

import java.net.URL;
import java.util.ResourceBundle;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */

public class ProductController implements Initializable {

    @FXML
    private TableView<Product> tvProduct;

    @FXML
    private TableColumn<Product, Integer> clProductId;

    @FXML
    private TableColumn<Product, String> clProdName;


    @FXML
    private Button btnAdd;

    @FXML
    private Button btnEdit;


    @FXML
    void add(ActionEvent event) {

    }

    @FXML
    void edit(ActionEvent event) {

    }

    @FXML
    void save(ActionEvent event) {

    }


    public void loadProductTable(){
        //create columns
        clProductId.setCellValueFactory(new PropertyValueFactory<>("ProductId"));
        clProdName.setCellValueFactory(new PropertyValueFactory<>("ProdName"));

        //load data to tvProduct
        tvProduct.setItems(ProductDB.getProducts());
        //add columns
        tvProduct.getColumns().addAll(clProductId,clProdName);

    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //lblTableName.setText("Product");
        tvProduct.getColumns().clear(); //clear all columns in the TableView

        //display the data in the TableView
        loadProductTable();
    }
}

