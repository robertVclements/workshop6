package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import sample.*;

import java.net.URL;
import java.util.ResourceBundle;


/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
public class ModifyCustomerController implements Initializable {

    private enum Models { add, edit};
    private Models model;
    private Customer origCust,newCust;
    private ObservableList <String> provinces = FXCollections.observableArrayList(
            "AB","BC","MB","NB","NL","NT","NS","NU","ON","PE","QC","SK","YT");

    @FXML
    private TextField tfCustFirstName;

    @FXML
    private TextField tfCustAddress;

    @FXML
    private TextField tfCustCountry;

    @FXML
    private TextField tfCustLastName;

    @FXML
    private TextField tfCustCity;

    @FXML
    private TextField tfCustPostal;

    @FXML
    private TextField tfCustHomePhone;

    @FXML
    private TextField tfCustEmail;

    @FXML
    private ComboBox<Agent> cmbAgent;

    @FXML
    private TextField tfCustBusPhone;

    @FXML
    private Button btnAction;

    @FXML
    private Button btnReset;

    @FXML
    private ComboBox<String> cmbCustProv;

    @FXML
    private Label lblMsgPostal;

    @FXML
    private Label lblMsgHomePhone;

    @FXML
    private Label lblMsgBusPhone;

    @FXML
    private Label lblMsgEmail;

    @FXML
    private Label lblMessage;



    @FXML
    void btnAction(ActionEvent event) {

        lblMessage.setText(null);
        lblMsgHomePhone.setText("*");
        lblMsgBusPhone.setText(null);
        lblMsgEmail.setText(null);
        lblMsgPostal.setText("*");
        if ((tfCustFirstName.getText().isEmpty())
                || (tfCustLastName.getText().isEmpty())
                || (tfCustAddress.getText().isEmpty())
                || (tfCustCity.getText().isEmpty())
                || (tfCustEmail.getText().isEmpty())
                || (tfCustHomePhone.getText().isEmpty())
                || (cmbCustProv.getValue() == null))
        {
            lblMessage.setText("Please fill in all the fields marked by *");
        }
        else if (!(Validation.isCanadaPostal(tfCustPostal.getText())))
        {
            lblMsgPostal.setText("Invalid Canada postal code.");
        }
        else if (!(Validation.isPhoneNo(tfCustHomePhone.getText())))
        {
            lblMsgHomePhone.setText("Incorrect formatted phone number.");
        }
        else if (!(Validation.isPhoneNo(tfCustBusPhone.getText())))
        {
            lblMsgBusPhone.setText("Incorrect formatted phone number.");
        }
        else if (!(Validation.isEmail(tfCustEmail.getText())))
        {
            lblMsgEmail.setText("Invalid email address.");
        }
        else
        {
            //create a new customer by passing the inputs, and assign a fake customerId
            newCust = new Customer(0, tfCustFirstName.getText(), tfCustLastName.getText(),tfCustAddress.getText(),
                    tfCustCity.getText(), cmbCustProv.getSelectionModel().getSelectedItem(), tfCustPostal.getText(),
                    tfCustCountry.getText(), tfCustHomePhone.getText(), tfCustBusPhone.getText(),tfCustEmail.getText(),
                    cmbAgent.getSelectionModel().getSelectedItem());

            // add the new customer
            if (model == Models.valueOf("add")) {
                //insert the new booking into the database and check if successes
                if (CustomerDB.addCustomer(newCust)) {

                    // get a handle to the stage
                    Stage stage = (Stage) btnAction.getScene().getWindow();
                    // close the stage
                    stage.close();
                } else
                    lblMessage.setText("Failed to add the new customer, please try again.");
            }
            // edit the original customer with the new one
            else if ( model == Models.valueOf("edit"))
            {
                if (CustomerDB.updateCustomer(origCust, newCust)) {
                    // get a handle to the stage
                    Stage stage = (Stage) btnAction.getScene().getWindow();
                    // close the stage
                    stage.close();
                } else
                    lblMessage.setText("Failed to edit the Customer, please try again.");
            }
        }
    }

    @FXML
    void btnRest(ActionEvent event) {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }

    /*The method will be called when user clicks the add button in the customerTable.fxml file,
     * so it will pass the login agent info to here
     */
    public void setDefaultOfAdding(Agent user){

        model = Models.add;
        cmbCustProv.getItems().addAll(provinces);
        tfCustCountry.setText("Canada");
        tfCustCountry.setEditable(false);
        cmbAgent.setValue(user); //the current agent will be assigned to the new adding customer
        cmbAgent.getItems().addAll(AgentDB.getAllAgents()); // the other agents could be options

    }

    //get the customer data which is needed to be edited
    public void setDefaultOfEditing(Customer c) {

        origCust = c;
        model = Models.edit;
        cmbCustProv.getItems().addAll(provinces);
        cmbCustProv.setValue(c.getCustProv());
        tfCustCountry.setText("Canada");
        tfCustCountry.setEditable(false);


        tfCustFirstName.setText(c.getCustFirstName());
        tfCustLastName.setText(c.getCustLastName());
        tfCustAddress.setText(c.getCustAddress());
        tfCustPostal.setText(c.getCustPostal());
        tfCustCity.setText(c.getCustCity());
        tfCustCountry.setText(c.getCustCountry());
        tfCustHomePhone.setText(c.getCustHomePhone());
        tfCustBusPhone.setText(c.getCustBusPhone());
        tfCustEmail.setText(c.getCustEmail());

        cmbAgent.getItems().addAll(AgentDB.getAllAgents());
        cmbAgent.setValue(c.getAgent());

    }

}

