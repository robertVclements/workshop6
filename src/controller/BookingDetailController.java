package controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import sample.*;

import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Currency;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */

public class BookingDetailController implements Initializable {

    //declare of the model of the BookingDetail page
    private enum Models { adding, editing, readOnly};
    private Models model;
    private BookingDetail origBkDetail; // the detail about the booking that the user clicked the button in
    private Booking selectedBooking;


    @FXML
    private AnchorPane acpBookingDetail;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnReset;

    @FXML
    private TextField tfItineraryNo;

    @FXML
    private TextField tfDestination;

    @FXML
    private TextField tfDescription;

    @FXML
    private DatePicker dpTripStart;

    @FXML
    private DatePicker dpTripEnd;

    @FXML
    private ComboBox<Region> cmbRegion;

    @FXML
    private TextField tfBasePrice;

    @FXML
    private TextField tfAgencyCommission;

    @FXML
    private ComboBox<TripClass> cmbClass;

    @FXML
    private ComboBox<Fee> cmbFee;

    @FXML
    private ComboBox<ProductSupplier> cmbProductSupplier;


    @FXML
    private Label lblMessage;


    @FXML
    void reset(ActionEvent event) {
        setDefaultView();
    }

    @FXML
    void save(ActionEvent event) {
        BookingDetail newData = getBookingDetailInputs();
        if (model == Models.valueOf("adding"))
        {
            if (!(BookingDetailDB.addBookingDetail(newData)))
                lblMessage.setText("Failed to add, please try again.");
            else
            {
                // get a handle to the stage
                Stage stage = (Stage) btnSave.getScene().getWindow();
                // close the stage
                stage.close();
            }
        }
        else if (model == Models.valueOf("editing"))
        {
            if (!BookingDetailDB.updateBookingDetail(origBkDetail, newData))
                lblMessage.setText("Failed to update, please try again.");
            else
            {
                // get a handle to the stage
                Stage stage = (Stage) btnSave.getScene().getWindow();
                // close the stage
                stage.close();
            }
        }

    }



    /* This method will be called when the BookingDetail button is clicked in the BookingController page,
    *  and pass the related booking object to here
    */
    public void initData(Booking b){
        //copy the passed booking data to the local variable
        selectedBooking = b;
        //get the related bookingDetail info
        origBkDetail = BookingDetailDB.getDetailByBookingId(selectedBooking.getBookingId());
        setDefaultView();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


    }

    private void setDefaultView()
    {
        //if there is no bookingDetail related the bookingId, then set model as adding.
        if ((selectedBooking.getBookingStatus().equals("Open")) && (origBkDetail == null))
        {
            model = Models.adding;
            acpBookingDetail.setDisable(false);
            //set all items for the combo boxes
            setAllItemsOfDetailCombobox();
            //set the default value for the combo boxes
            cmbRegion.getSelectionModel().selectFirst();
            cmbClass.getSelectionModel().selectFirst();
            cmbFee.getSelectionModel().selectFirst();
            cmbProductSupplier.getSelectionModel().selectFirst();

        }
        else if (selectedBooking.getBookingStatus().equals("Closed") || selectedBooking.getBookingStatus().equals("Canceled")){
            model = Models.readOnly;
            acpBookingDetail.setDisable(true);
            btnReset.setVisible(false);
            btnSave.setVisible(false);
            fillInOrigBookingDetail();
        }
        else {
            model = Models.editing;
            acpBookingDetail.setDisable(false);
            //set all items for the combo boxes
            setAllItemsOfDetailCombobox();
            //fill out the selectedDetail info for editing
            fillInOrigBookingDetail();
        }

    }

    private void setAllItemsOfDetailCombobox()
    {
        cmbRegion.getItems().addAll(RegionDB.getAllRegions());
        cmbClass.getItems().addAll(TripClassDB.getAllClasses());
        cmbFee.getItems().addAll(FeeDB.getAllFee());
        cmbProductSupplier.getItems().addAll(ProductSupplierDB.getAllProductSuppliers());
    }

    private void fillInOrigBookingDetail() {
        tfItineraryNo.setText((Integer.toString((int)origBkDetail.getItineraryNo()))); // first convert to int, then to String
        dpTripStart.setValue(origBkDetail.getTripStart());
        dpTripEnd.setValue(origBkDetail.getTripEnd());
        tfDescription.setText(origBkDetail.getDescription());
        tfDestination.setText(origBkDetail.getDestination());
        //use the below format to display money
        DecimalFormat df = new DecimalFormat("###,###.00");
        tfBasePrice.setText(df.format(origBkDetail.getBasePrice()));
        tfAgencyCommission.setText(df.format(origBkDetail.getAgencyCommission()));
        cmbRegion.setValue(RegionDB.getRegionById(origBkDetail.getRegionId()));
        cmbClass.setValue(TripClassDB.getClassById(origBkDetail.getClassId()));
        cmbFee.setValue(FeeDB.getFeeById(origBkDetail.getFeeId()));
        cmbProductSupplier.setValue(ProductSupplierDB.getProductSupplierById(origBkDetail.getProductSupplierId()));
    }

    private BookingDetail getBookingDetailInputs() {
        //fake detail Id
        BookingDetail newBkDetail = new BookingDetail(0,Integer.parseInt(tfItineraryNo.getText()),
                dpTripStart.getValue(),dpTripEnd.getValue(),tfDescription.getText(),tfDestination.getText(),new BigDecimal(tfBasePrice.getText()),
                new BigDecimal(tfAgencyCommission.getText()),selectedBooking.getBookingId(),cmbRegion.getSelectionModel().getSelectedItem().getRegionId(),
                cmbClass.getSelectionModel().getSelectedItem().getClassId(),cmbFee.getSelectionModel().getSelectedItem().getFeeId(),
                cmbProductSupplier.getSelectionModel().getSelectedItem().getProductSupplierId());
        return newBkDetail;
        /*newBkDetail
        newBkDetail.setBookingDetailId(0);
        newBkDetail.setItineraryNo();
        newBkDetail.setTripStart();
        newBkDetail.setTripEnd();
        newBkDetail.setDescription();
        newBkDetail.setDestination();
        newBkDetail.setBasePrice();
        newBkDetail.setAgencyCommission();
        newBkDetail.setBookingId();
        newBkDetail.setRegionId();
        newBkDetail.setClassId(;
        newBkDetail.setFeeId(cmbFee.getSelectionModel().getSelectedItem().getFeeId());
        newBkDetail.setProductSupplierId();*/
    }




}
