package controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.*;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;


/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */

public class BookingController implements Initializable {

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnEdit;

    @FXML
    private TableView<Booking> tvBooking;

    @FXML
    private TableColumn<Booking, Integer> clBookingId;

    @FXML
    private TableColumn<Booking, LocalDate> clBookingDate;

    @FXML
    private TableColumn<Booking, String> clBookingNo;

    @FXML
    private TableColumn<Booking, Integer> clTravelerCount;

    @FXML
    private TableColumn<Booking, Customer> clCustomerId;

    @FXML
    private TableColumn<Booking, TripType> clTripType;

    @FXML
    private TableColumn<Booking, TravelPackage> clPackage;

    @FXML
    private TableColumn<Booking, String> clBookingStatus;

    @FXML
    private TableColumn<Booking, Button> clButBookingDetail;

    @FXML
    private TableColumn<Booking, Button> clButInvoice;

    @FXML
    void addNewBooking(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("../fxml/modifyBooking.fxml"));
        Parent add = loader.load();

        Scene sceneAdding = new Scene(add);
        sceneAdding.getStylesheets().add("/styleSheet/addData.css");

        Stage windowAdding = new Stage();
        windowAdding.setTitle("Add New Booking");
        windowAdding.setScene(sceneAdding);

        ModifyBookingController controller = loader.getController();
        controller.setDefaultOfAdding();

        windowAdding.showAndWait(); // need wait util the windowAdding closed
        tvBooking.setItems(BookingDB.getAllBookings()); //refresh the tvBooking
    }


    @FXML
    void editBooking(ActionEvent event) throws IOException {

        Booking selectedBooking = tvBooking.getSelectionModel().getSelectedItem();
        //the booking whose status is Closed and Canceled are not editable
        if (selectedBooking == null )
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            //alert.setTitle("Error Message");
            //alert.setHeaderText("Results:");
            alert.setContentText("Please select a booking that the Status is not Closed or Canceled.");

            alert.showAndWait();
        }
        else if (selectedBooking.getBookingStatus().equals("Closed") || selectedBooking.getBookingStatus().equals("Canceled"))
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            //alert.setTitle("Error Message");
            //alert.setHeaderText("Results:");
            alert.setContentText("Closed or Canceled bookings are not editable.");

            alert.showAndWait();
        }
        else
        {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../fxml/modifyBooking.fxml"));
            Parent subRoot = loader.load();

            Scene sceneEditing = new Scene(subRoot);
            sceneEditing.getStylesheets().add("/styleSheet/addData.css");

            Stage windowEditing = new Stage();
            windowEditing.setTitle("Edit the Booking");
            windowEditing.setScene(sceneEditing);

            ModifyBookingController controller = loader.getController();
            controller.setDefaultOfEditing(selectedBooking);

            windowEditing.showAndWait();// need wait util the windowAdding closed
            tvBooking.setItems(BookingDB.getAllBookings()); //refresh the tvBooking
        }


    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //clears the TableView preparing for loading latest data
        tvBooking.getColumns().clear();

        //loads the data from database to the TableView
        loadBookingTable();



    }


    //The method is to create booking columns in the table view, get and shows the data from the database
    private void loadBookingTable(){
        //create columns
        clBookingId.setCellValueFactory(new PropertyValueFactory<>("bookingId"));
        clBookingDate.setCellValueFactory(new PropertyValueFactory<>("bookingDate"));
        clBookingNo.setCellValueFactory(new PropertyValueFactory<>("bookingNo"));
        clTravelerCount.setCellValueFactory(new PropertyValueFactory<>("travelerCount"));
        clCustomerId.setCellValueFactory(new PropertyValueFactory<>("customerId"));
        clTripType.setCellValueFactory(new PropertyValueFactory<>("tripType"));
        clPackage.setCellValueFactory(new PropertyValueFactory<>("travelPackage"));
        clBookingStatus.setCellValueFactory(new PropertyValueFactory<>("bookingStatus"));

        /*
        use customized TableCell class(ActionButtonTableCell) to add button in a table cell;
        the action of the button is to open BookingDetail window and pass the booking data
         */
        clButBookingDetail.setCellFactory(ActionButtonTableCell.forTableColumn("Booking Detail", (Booking b) ->{

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/bookingDetail.fxml"));
            try {
                Parent detailPage = loader.load();

                Scene sceneDetail = new Scene(detailPage);
                sceneDetail.getStylesheets().add("/styleSheet/addData.css");

                Stage windowAdding = new Stage();
                windowAdding.setTitle("Booking Detail");
                windowAdding.setScene(sceneDetail);

                BookingDetailController controller = loader.getController();
                controller.initData(b);

                windowAdding.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return b;

        }));

        /*
        use customized TableCell class(ActionButtonTableCell) to add button in a table cell;
        the action of the button is to open Invoice window and pass the booking data
         */
        clButInvoice.setCellFactory(ActionButtonTableCell.forTableColumn("Invoice", (Booking b) ->{

           /* FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/bookingDetail.fxml"));
            try {
                Parent detailPage = loader.load();

                Scene sceneDetail = new Scene(detailPage);
                sceneDetail.getStylesheets().add("/styleSheet/addData.css");

                Stage windowAdding = new Stage();
                windowAdding.setTitle("Booking Detail");
                windowAdding.setScene(sceneDetail);

                BookingDetailController controller = loader.getController();
                controller.initData(b);

                windowAdding.show();
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            return b;

        }));

        ObservableList<Booking> allBookings = FXCollections.observableArrayList();
        allBookings = BookingDB.getAllBookings();
        /*for (var b : allBookings) {

            if (b.getBookingStatus())
            //get the bookingDetail object for the selectedBooking
            BookingDetail selectedDetail = BookingDetailDB.getBookingDetailById(selectedBooking.getBookingId());
        }*/


        //load data to tvBooking
        tvBooking.setItems(allBookings);
        //add columns
        tvBooking.getColumns().addAll(clBookingId, clBookingDate, clBookingNo, clTravelerCount,
                clCustomerId, clTripType, clPackage, clBookingStatus, clButBookingDetail, clButInvoice);

        //make descending of bookingId as default sort order of booking table view
        clBookingId.setSortType(TableColumn.SortType.DESCENDING);
        tvBooking.getSortOrder().add(clBookingId);

    }

    //This method is to get all customerId as a list from the database and return the list
    public ObservableList<Integer> getAllCustomerIds() {
        //prepare for customerId combo box
        ObservableList<Customer> customers = FXCollections.observableArrayList();
        ObservableList<Integer> custIds = FXCollections.observableArrayList();
        customers.addAll(CustomerDB.getAllCustomers());
        for (Customer cust : customers) {
            custIds.add(cust.getCustomerId());
        }
        return custIds;
    }

    public ObservableList<String> getAllTripTypeIds() {
        //prepare for customerId combo box
        ObservableList<TripType> tripTypes = FXCollections.observableArrayList();
        ObservableList<String> tripTypeIds = FXCollections.observableArrayList();
        tripTypes.addAll(TripTypeDB.getTripTypes());
        for (TripType ttype : tripTypes) {
            tripTypeIds.add(ttype.getTripTypeId());
        }
        return tripTypeIds;
    }


}
