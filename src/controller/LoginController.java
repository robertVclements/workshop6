package controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Agent;
import sample.AgentDB;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
public class LoginController implements Initializable {

    @FXML
    private TextField tfUserName;

    @FXML
    private PasswordField pfPassword;

    @FXML
    private Button btnLogin;

    @FXML
    private Button btnForgotPassword;

    @FXML
    private Label lblCompanyName;

    @FXML
    private Label lblMessage;

    @FXML
    // login button clicked
    void login(ActionEvent event) throws IOException {

        Agent user = null;
        ObservableList<Agent>  agents = AgentDB.getAllAgents();
        // check if the login information are valid or not
        for(var agent : agents)
        {
            if (agent.getAgtEmail().equals(tfUserName.getText()) && agent.getAgtPassword().equals(pfPassword.getText()))
            {
                user = agent;
                break;
            }
        }

        if (user != null) //go to main page, if the user exists in the database
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/sample.fxml"));
            Parent samplePage = loader.load();

            Stage sampleWindow = (Stage)((Node) event.getSource()).getScene().getWindow();
            sampleWindow.setScene(new Scene(samplePage));
            sampleWindow.setMaximized(true);
            Scene sampleScene = sampleWindow.getScene();
            sampleScene.getStylesheets().add(getClass().getResource("/styleSheet/sample.css").toExternalForm());

            SampleController controller = loader.getController();
            controller.userInfo(user);

            sampleWindow.show();
        }
        else {
            lblMessage.setText("Invalid UserName or Password, please try again.");
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

}