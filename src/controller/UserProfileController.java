package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import sample.Agent;
import sample.AgentDB;
import sample.Validation;

import java.net.URL;
import java.util.ResourceBundle;

public class UserProfileController implements Initializable {

    Agent loginUser = null;


    @FXML
    private Button btnSavePwd;

    @FXML
    private Button btnCancel;

    @FXML
    private PasswordField pfPassword;

    @FXML
    private PasswordField pfPwdConfirm;

    @FXML
    private PasswordField pfNewPassword;

    @FXML
    private Label lblUserName;

    @FXML
    private Label lblMessage;

    @FXML
    private Label lblNote;

    @FXML
    void cancel(ActionEvent event) {
        pfPassword.setText(null);
        pfNewPassword.setText(null);
        pfPwdConfirm.setText(null);
        lblMessage.setText(null);
    }

    @FXML
    void save(ActionEvent event) {

        if ((pfNewPassword.getText() == null) || (pfNewPassword.getText().isEmpty())
               || (pfPassword.getText() == null) || (pfPassword.getText().isEmpty())
                || (pfPwdConfirm.getText() == null) || (pfPwdConfirm.getText().isEmpty()))
        {
            lblMessage.setText("Please fill out all the fields.");
        }
        else if (!(loginUser.getAgtPassword().equals(pfPassword.getText())))
        {
            lblMessage.setText("The password is invalid, please try again.");
        }
        else if (!(Validation.isPassword(pfNewPassword.getText()))){
            lblMessage.setText("The new password should meet the requirement.");
        }
        else if (!(pfPwdConfirm.getText().equals(pfNewPassword.getText())))
        {
            lblMessage.setText("The password confirm is incorrect, please enter again.");
        }
        else
        {
            loginUser.setAgtPassword(pfNewPassword.getText());
            if (AgentDB.resetPassword(loginUser)) {
                lblMessage.setText("Reset the password successfully!");
            }
            else lblMessage.setText("Failed to reset the password!");
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        lblNote.setText("The new password must contains: \n " +
                "1. At least 8 characters \n" +
                " 2. At least one digit \n" +
                " 3. At least one upper case letter \n" +
                " 4. No whitespace allowed.");

    }

    public void initData(Agent user) {
        loginUser = user;
        lblUserName.setText(loginUser.getAgtEmail());
    }
}
