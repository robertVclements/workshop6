package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
public class ProductDB {


    public static ObservableList<Product> getProducts() {

        ObservableList<Product>products = FXCollections.observableArrayList();
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from products" );
            while (rs.next()) {
                products.add(new Product(rs.getInt(1), rs.getString(2)));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }

    public static Product getProductById(int id) {

        Product product = null;
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from products where ProductId = " + id );
            while (rs.next()) {
                product = new Product(rs.getInt(1), rs.getString(2));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

}
