package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.math.BigDecimal;
import java.sql.*;

public class BookingDetailDB {

    //gets Booking data from database and stores in a list and return the list
    public static BookingDetail getDetailByBookingId(int id) {

        BookingDetail bookingDetail= null;
        Connection conn = DBConnection.getDBConnection();
        String query = "select * from bookingdetails where BookingId =" + id;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                bookingDetail = new BookingDetail(
                        rs.getInt(1),
                        rs.getDouble(2),
                        rs.getDate(3).toLocalDate(),
                        rs.getDate(4).toLocalDate(),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getBigDecimal(7),
                        rs.getBigDecimal(8),
                        rs.getInt(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getInt(13));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookingDetail;
    }

    public static boolean addBookingDetail(BookingDetail bd){
        Connection conn = DBConnection.getDBConnection();
        boolean result = false;
        PreparedStatement pst = null;
        String query = "INSERT INTO bookingdetails (ItineraryNo, TripStart,TripEnd,Description,Destination," +
                "BasePrice, AgencyCommission, BookingId,RegionId,ClassId,FeeId,ProductSupplierId) values(?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            pst = conn.prepareStatement(query);
            pst.setDouble(1, bd.getItineraryNo());
            pst.setDate(2,java.sql.Date.valueOf(bd.getTripStart()));
            pst.setDate(3,java.sql.Date.valueOf(bd.getTripEnd()));
            pst.setString(4,bd.getDescription());
            pst.setString(5,bd.getDestination());
            pst.setBigDecimal(6,bd.getBasePrice());
            pst.setBigDecimal(7,bd.getAgencyCommission());
            pst.setInt(8,bd.getBookingId());
            pst.setString(9,bd.getRegionId());
            pst.setString(10,bd.getClassId());
            pst.setString(11,bd.getFeeId());
            pst.setInt(12,bd.getProductSupplierId());
            //execute the statement and check if successes
            if (pst.executeUpdate() > 0);
                result = true;
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean updateBookingDetail(BookingDetail origData, BookingDetail newData){
        Connection conn = DBConnection.getDBConnection();
        boolean result = false;
        PreparedStatement pst = null;
        String query = "UPDATE bookingdetails SET ItineraryNo=?, TripStart=?,TripEnd=?,Description=?,Destination=?, " +
                "BasePrice =?, AgencyCommission =?, BookingId=?,RegionId=?,ClassId=?,FeeId=?,ProductSupplierId=?" +
                "where bookingDetailId =?, ItneraryNo=?, TripStar=?,TripEnd=?,Description=?,Destination=?, " +
                "BasePrice =?, AgencyCommission =?, BookingId=?,RegionId=?,ClassId=?,FeeId=?,ProductSupplierId=?";
        try {
            pst = conn.prepareStatement(query);
            //set new data
            pst.setDouble(1, newData.getItineraryNo());
            pst.setDate(2,java.sql.Date.valueOf(newData.getTripStart()));
            pst.setDate(3,java.sql.Date.valueOf(newData.getTripEnd()));
            pst.setString(4,newData.getDescription());
            pst.setString(5,newData.getDestination());
            pst.setBigDecimal(6,newData.getBasePrice());
            pst.setBigDecimal(7,newData.getAgencyCommission());
            pst.setInt(8,newData.getBookingId());
            pst.setString(9,newData.getRegionId());
            pst.setString(10,newData.getClassId());
            pst.setString(11,newData.getFeeId());
            pst.setInt(12,newData.getProductSupplierId());
            //use the original data
            pst.setInt(1, origData.getBookingDetailId());
            pst.setDouble(1, origData.getItineraryNo());
            pst.setDate(2,java.sql.Date.valueOf(origData.getTripStart()));
            pst.setDate(3,java.sql.Date.valueOf(origData.getTripEnd()));
            pst.setString(4,origData.getDescription());
            pst.setString(5,origData.getDestination());
            pst.setBigDecimal(6,origData.getBasePrice());
            pst.setBigDecimal(7,origData.getAgencyCommission());
            pst.setInt(8,origData.getBookingId());
            pst.setString(9,origData.getRegionId());
            pst.setString(10,origData.getClassId());
            pst.setString(11,origData.getFeeId());
            pst.setInt(12,origData.getProductSupplierId());
            //execute the statement and check if successes
            if (pst.executeUpdate() > 0);
            result = true;
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}