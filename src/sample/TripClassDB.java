package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TripClassDB {

    public static ObservableList<TripClass> getAllClasses() {
        ObservableList<TripClass> tripClasses = FXCollections.observableArrayList();
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from classes");
            while (rs.next()) {
                tripClasses.add(new TripClass( rs.getString(1),rs.getString(2),rs.getString(3)) );
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tripClasses;
    }

    public static TripClass getClassById(String classId) {
        TripClass tripClass = null;
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from classes where ClassId = '" + classId + "'");
            while (rs.next()) {
                tripClass = new TripClass( rs.getString(1),rs.getString(2),rs.getString(3) );
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tripClass;
    }
}
