package sample;

import java.math.BigDecimal;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
public class Fee {

    private String feeId;
    private String feeName;
    private BigDecimal feeAmt;
    private String feeDesc;

    public Fee(String feeId, String feeName, BigDecimal feeAmt, String feeDesc) {
        this.feeId = feeId;
        this.feeName = feeName;
        this.feeAmt = feeAmt;
        this.feeDesc = feeDesc;
    }

    public String getFeeId() {
        return feeId;
    }

    public void setFeeId(String feeId) {
        this.feeId = feeId;
    }

    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    public BigDecimal getFeeAmt() {
        return feeAmt;
    }

    public void setFeeAmt(BigDecimal feeAmt) {
        this.feeAmt = feeAmt;
    }

    public String getFeeDesc() {
        return feeDesc;
    }

    public void setFeeDesc(String feeDesc) {
        this.feeDesc = feeDesc;
    }

    @Override
    public String toString() {
        return feeName;
    }
}
