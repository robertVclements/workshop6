package sample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
//create a Singleton Class
public class DBConnection {

    static Connection conn = null;

    //make the constructor private so that this class cannot be instantiated
    private DBConnection(){}

    //Get the only object available
    public static Connection getDBConnection() {
        try {
            if ((conn == null) || (conn.isClosed())) {
                Class.forName("com.mysql.jdbc.Driver"); //might throw ClassNotFoundException //need add referenced library
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/travelexperts", "root", ""); //might throw SQLException
            }
        } catch(ClassNotFoundException | SQLException e){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return conn;
    }
}
