package sample;

import controller.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
public class Main extends Application {

   /* @Override
   //login page
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/login.fxml"));
        primaryStage.setTitle("Travel Experts");
        Scene LoginScene = new Scene(root, 600, 400);
        LoginScene.getStylesheets().add(getClass().getResource("/styleSheet/login.css").toExternalForm());
        primaryStage.setScene(LoginScene);
        primaryStage.show();

    }*/



    @Override
    //main page(sample page)
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/sample.fxml"));
        primaryStage.setTitle("Travel Experts");
        Scene scene = new Scene(root);

        scene.getStylesheets().add(getClass().getResource("/styleSheet/sample.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.show();

    }

    /*@Override
    //main page(sample page)
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/bookingDetail.fxml"));
        primaryStage.setTitle("Travel Experts");
        Scene scene = new Scene(root);

        //scene.getStylesheets().add(getClass().getResource("/styleSheet/sample.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.show();

    }*/


    public static void main(String[] args) {
        launch(args);
    }


}
