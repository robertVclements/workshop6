package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */

public class TripTypeDB {

    //gets Booking data from database and stores in a list and return the list
    public static ObservableList<TripType> getTripTypes() {
        ObservableList<TripType> tripTypes = FXCollections.observableArrayList();
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from triptypes");
            while (rs.next()) {
                tripTypes.add(new TripType( rs.getString(1),rs.getString(2) ));
            }
            rs.close();
            //conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tripTypes;

    }


    public static TripType getTripTypeById(String typeId) {
        TripType tripType = null;
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from triptypes where TripTypeId = '" + typeId + "'");
            while (rs.next()) {
                tripType = new TripType( rs.getString(1),rs.getString(2) );
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tripType;

    }
}
