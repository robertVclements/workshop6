package sample;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SupplierDB {

    public static Supplier getSupplierById(int id) {

        Supplier supp = null;
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from suppliers where SupplierId = " + id );
            while (rs.next()) {
                supp = new Supplier(rs.getInt(1), rs.getString(2));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return supp;
    }
}
