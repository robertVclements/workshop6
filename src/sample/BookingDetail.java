package sample;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.math.BigDecimal;
import java.time.LocalDate;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
public class BookingDetail {

    private int bookingDetailId;
    private double itineraryNo;
    private LocalDate tripStart;
    private LocalDate tripEnd;
    private String description;
    private String destination;
    private BigDecimal basePrice;
    private BigDecimal agencyCommission;
    private int bookingId;
    private String regionId;
    private String classId;
    private String feeId;
    private int productSupplierId;

    public BookingDetail(int bookingDetailId, double itineraryNo, LocalDate tripStart, LocalDate tripEnd, String description,
                         String destination, BigDecimal basePrice, BigDecimal agencyCommission, int bookingId,
                         String regionId, String classId, String feeId, int productSupplierId) {
        this.bookingDetailId = bookingDetailId;
        this.itineraryNo = itineraryNo;
        this.tripStart = tripStart;
        this.tripEnd = tripEnd;
        this.description = description;
        this.destination = destination;
        this.basePrice = basePrice;
        this.agencyCommission = agencyCommission;
        this.bookingId = bookingId;
        this.regionId = regionId;
        this.classId = classId;
        this.feeId = feeId;
        this.productSupplierId = productSupplierId;
    }

    public int getBookingDetailId() {
        return bookingDetailId;
    }

    public void setBookingDetailId(int bookingDetailId) {
        this.bookingDetailId = bookingDetailId;
    }

    public double getItineraryNo() {
        return itineraryNo;
    }

    public void setItineraryNo(double itineraryNo) {
        this.itineraryNo = itineraryNo;
    }

    public LocalDate getTripStart() {
        return tripStart;
    }

    public void setTripStart(LocalDate tripStart) {
        this.tripStart = tripStart;
    }

    public LocalDate getTripEnd() {
        return tripEnd;
    }

    public void setTripEnd(LocalDate tripEnd) {
        this.tripEnd = tripEnd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getAgencyCommission() {
        return agencyCommission;
    }

    public void setAgencyCommission(BigDecimal agencyCommission) {
        this.agencyCommission = agencyCommission;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getFeeId() {
        return feeId;
    }

    public void setFeeId(String feeId) {
        this.feeId = feeId;
    }

    public int getProductSupplierId() {
        return productSupplierId;
    }

    public void setProductSupplierId(int productSupplierId) {
        this.productSupplierId = productSupplierId;
    }
}
