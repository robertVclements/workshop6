package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */

public class BookingDB {


    //gets Booking data from database and stores in a list and return the list
    public static ObservableList<Booking> getAllBookings() {
        ObservableList<Booking> bookings = FXCollections.observableArrayList();
        Connection conn = DBConnection.getDBConnection();
        String query = "select * from bookings";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                bookings.add(new Booking(rs.getInt(1),
                        rs.getDate(2).toLocalDate(),
                        rs.getString(3),
                        (int)rs.getDouble(4),
                        rs.getInt(5),
                        TripTypeDB.getTripTypeById(rs.getString(6)),
                        TravelPackageDB.getTravelPackageById(rs.getInt(7)),
                        rs.getString(8)));
            }
            rs.close();
            //conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookings;
    }

    public static Booking getBookingById(int bookingId) {

        Booking booking = null;
        Connection conn = DBConnection.getDBConnection();
        String query = "select * from bookings where BookingId = " + bookingId;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                booking = new Booking(rs.getInt(1),
                        rs.getDate(2).toLocalDate(),
                        rs.getString(3),
                        (int)rs.getDouble(4),
                        rs.getInt(5),
                        TripTypeDB.getTripTypeById(rs.getString(6)),
                        TravelPackageDB.getTravelPackageById(rs.getInt(7)),
                        rs.getString(8));
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return booking;
    }

    //Inserts a new booking data into the database. If succeeded, return true, otherwise, return false.
    public static boolean addBooking(Booking b) {
        boolean result = false;
        ObservableList<Booking> bookings = FXCollections.observableArrayList();
        Connection conn = DBConnection.getDBConnection();
        PreparedStatement pst = null;
        //Statement stmt = conn.createStatement();
        String query = "INSERT INTO bookings (BookingDate, BookingNo, TravelerCount, CustomerId, TripTypeId, PackageId, BookingStatus) values(?,?,?,?,?,?,?)";
        try {
            pst = conn.prepareStatement(query);
            pst.setDate(1, java.sql.Date.valueOf(b.getBookingDate()));
            pst.setString(2, b.getBookingNo());
            pst.setDouble(3, (double)b.getTravelerCount());
            pst.setInt(4, b.getCustomerId());
            pst.setString(5, b.getTripType().getTripTypeId());
            pst.setInt(6, b.getTravelPackage().getPackageId());
            pst.setString(7, b.getBookingStatus());
            //execute the statement and check if successes
            result = pst.execute();
            pst.close();
            //conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    //update a booking data into the database
    public static boolean updateBooking(Booking origBooking, Booking newBooking) {
        boolean result = false;
        Connection conn = DBConnection.getDBConnection();
        try {
            String query = "UPDATE bookings SET BookingNo=?, TravelerCount=?, CustomerId=?, TripTypeId=?, PackageId=?, BookingStatus=? " +
                    "where BookingId=? and BookingDate=? and BookingNo=? and TravelerCount=? and CustomerId=? and TripTypeId=? and PackageId=? and BookingStatus=?";
            //set new booking data for editing
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, newBooking.getBookingNo());
            pst.setDouble(2, (double)newBooking.getTravelerCount());
            pst.setInt(3, newBooking.getCustomerId());
            pst.setString(4, newBooking.getTripType().getTripTypeId());
            pst.setInt(5, newBooking.getTravelPackage().getPackageId());
            pst.setString(6, newBooking.getBookingStatus());

            //set original booking data to the sql query of where clause
            pst.setInt(7, origBooking.getBookingId());
            pst.setDate(8, java.sql.Date.valueOf(origBooking.getBookingDate()));
            pst.setString(9, origBooking.getBookingNo());
            pst.setDouble(10, (double)origBooking.getTravelerCount());
            pst.setInt(11, origBooking.getCustomerId());
            pst.setString(12, origBooking.getTripType().getTripTypeId());
            pst.setInt(13, origBooking.getTravelPackage().getPackageId());
            pst.setString(14, origBooking.getBookingStatus());
            //execute the statement and check if successes
            if ( pst.executeUpdate() >0 )
                result = true;
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;

    }

}

