package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.Collection;

/*Author: Lindsay
Date: October, 2018
Purpose: student project
*/
public class CustomerDB {
    public static ObservableList<Customer> getAllCustomers(){
        Connection conn = DBConnection.getDBConnection();
        ObservableList<Customer> customers = FXCollections.observableArrayList();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from customers");
            while(rs.next()) {
                customers.add(new Customer(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        AgentDB.getAgentById(rs.getInt(12))));
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public static Customer getCustomerById(int custId){
        Connection conn = DBConnection.getDBConnection();
        Customer customerData = null;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from customers where CustomerId=" + custId);
            while(rs.next()) {
                customerData = new Customer(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        AgentDB.getAgentById(rs.getInt(12)));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerData;
    }

    public static boolean addCustomer(Customer c){
        Connection conn = DBConnection.getDBConnection();
        boolean result = false;
        PreparedStatement pst = null;
        String query = "INSERT INTO customers (CustFirstName, CustLastName, CustAddress, CustCity, CustProv, CustPostal, " +
                "CustCountry, CustHomePhone, CustBusPhone, CustEmail, AgentId) values(?,?,?,?,?,?,?,?,?,?,?)";
        try {
            pst = conn.prepareStatement(query);
            pst.setString(1, c.getCustFirstName());
            pst.setString(2,c.getCustLastName());
            pst.setString(3,c.getCustAddress());
            pst.setString(4,c.getCustCity());
            pst.setString(5,c.getCustProv());
            pst.setString(6,c.getCustPostal());
            pst.setString(7,c.getCustCountry());
            pst.setString(8,c.getCustHomePhone());
            pst.setString(9,c.getCustBusPhone());
            pst.setString(10,c.getCustEmail());
            pst.setInt(11,c.getAgent().getAgentId());

            //execute the statement and check if successes
            if (pst.executeUpdate() > 0);
                result = true;
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean updateCustomer(Customer origCust, Customer newCust){
        Connection conn = DBConnection.getDBConnection();
        boolean result = false;
        PreparedStatement pst = null;
        String query = "update customers set CustFirstName=?, CustLastName=?, CustAddress=?, CustCity=?, CustProv=?, CustPostal=?, " +
                "CustCountry=?, CustHomePhone=?, CustBusPhone=?, CustEmail=?, AgentId=? where CustomerId=? and CustFirstName=? " +
                "and CustLastName=? and CustAddress=? and CustCity=? and CustProv=? and CustPostal=? and CustCountry=? " +
                "and CustHomePhone=? and CustBusPhone=? and CustEmail=? and AgentId=?";
        try {
            pst = conn.prepareStatement(query);
            //set newCust data
            pst.setString(1, newCust.getCustFirstName());
            pst.setString(2,newCust.getCustLastName());
            pst.setString(3,newCust.getCustAddress());
            pst.setString(4,newCust.getCustCity());
            pst.setString(5,newCust.getCustProv());
            pst.setString(6,newCust.getCustPostal());
            pst.setString(7,newCust.getCustCountry());
            pst.setString(8,newCust.getCustHomePhone());
            pst.setString(9,newCust.getCustBusPhone());
            pst.setString(10,newCust.getCustEmail());
            pst.setInt(11,newCust.getAgent().getAgentId());
            //set origCust data
            pst.setInt(12,origCust.getCustomerId());
            pst.setString(13, origCust.getCustFirstName());
            pst.setString(14,origCust.getCustLastName());
            pst.setString(15,origCust.getCustAddress());
            pst.setString(16,origCust.getCustCity());
            pst.setString(17,origCust.getCustProv());
            pst.setString(18,origCust.getCustPostal());
            pst.setString(19,origCust.getCustCountry());
            pst.setString(20,origCust.getCustHomePhone());
            pst.setString(21,origCust.getCustBusPhone());
            pst.setString(22,origCust.getCustEmail());
            pst.setInt(23,origCust.getAgent().getAgentId());

            //execute the statement and check if successes
            if (pst.executeUpdate() > 0);
                result = true;
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
