/*package sample;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class CustomerTableView {

    private SimpleIntegerProperty customerId;
    private String custFirstName;
    private String custLastName;
    private String custAddress;
    private String custCity;
    private String custProv;
    private String custPostal;
    private String custCountry;
    private String custHomePhone;
    private String custBusPhone;
    private String custEmail;
    private SimpleObjectProperty<Agent> agent;

    public CustomerTableView(int customerId, String custFirstName, String custLastName, String custAddress,
                    String custCity, String custProv, String custPostal, String custCountry, String custHomePhone,
                    String custBusPhone, String custEmail, Agent agent) {
        this.customerId = new SimpleIntegerProperty(customerId);
        this.custFirstName = custFirstName;
        this.custLastName = custLastName;
        this.custAddress = custAddress;
        this.custCity = custCity;
        this.custProv = custProv;
        this.custPostal = custPostal;
        this.custCountry = custCountry;
        this.custHomePhone = custHomePhone;
        this.custBusPhone = custBusPhone;
        this.custEmail = custEmail;
        this.agent = new SimpleObjectProperty<Agent> (agent);
    }

    public int getCustomerId() {
        return customerId.get();
    }

    public void setCustomerId(int customerId) {
        this.customerId.set(customerId);
    }

    public String getCustFirstName() {
        return custFirstName;
    }

    public void setCustFirstName(String custFirstName) {
        this.custFirstName = custFirstName;
    }

    public String getCustLastName() {
        return custLastName;
    }

    public void setCustLastName(String custLastName) {
        this.custLastName = custLastName;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustProv() {
        return custProv;
    }

    public void setCustProv(String custProv) {
        this.custProv = custProv;
    }

    public String getCustPostal() {
        return custPostal;
    }

    public void setCustPostal(String custPostal) {
        this.custPostal = custPostal;
    }

    public String getCustCountry() {
        return custCountry;
    }

    public void setCustCountry(String custCountry) {
        this.custCountry = custCountry;
    }

    public String getCustHomePhone() {
        return custHomePhone;
    }

    public void setCustHomePhone(String custHomePhone) {
        this.custHomePhone = custHomePhone;
    }

    public String getCustBusPhone() {
        return custBusPhone;
    }

    public void setCustBusPhone(String custBusPhone) {
        this.custBusPhone = custBusPhone;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public Agent getAgent() {
        return agent.get();
    }
    public void setAgent(Agent agent) {
        this.agent.set(agent);
    }

    //This method is to parse a CustomerTableView object to a Customer object
    public Customer toCustomer(){
        Customer c = null;
        c.setCustomerId(customerId.get());
        c.setCustFirstName(custFirstName);
        c.setCustLastName(custLastName);
        c.setCustAddress(custAddress);
        c.setCustCity(custCity);
        c.setCustProv(custProv);
        c.setCustPostal(custPostal);
        c.setCustHomePhone(custHomePhone);
        c.setCustBusPhone(custBusPhone);
        c.setCustEmail(custEmail);
        c.setAgentId(agent.get().getAgentId()); //agent.get() is to get agent object
        return c;
    }

}*/
