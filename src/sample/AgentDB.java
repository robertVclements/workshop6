package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
public class AgentDB {
        public static ObservableList<Agent> getAllAgents() {
            Connection conn = DBConnection.getDBConnection();
            ObservableList<Agent> agents = FXCollections.observableArrayList();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("select * from agents");
                while (rs.next()) {
                    agents.add(new Agent(
                            rs.getInt(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getString(4),
                            rs.getString(5),
                            rs.getString(6),
                            rs.getString(7),
                            rs.getString(8),
                            rs.getString(9)));
                }
                rs.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return agents;
        }

    public static Agent getAgentById(int id) {
        Connection conn = DBConnection.getDBConnection();
        Agent agent = null;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from agents where AgentId =" + id);
            while (rs.next()) {
                agent = new Agent(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9));
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return agent;
    }

    public static boolean resetPassword(Agent user) {
        boolean result = false;
        Connection conn = DBConnection.getDBConnection();
        try {
            String query = "UPDATE agents SET AgtPassword=? where AgentId=?";
            //set new booking data for editing
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, user.getAgtPassword());
            pst.setInt(2, user.getAgentId());

            //execute the statement and check if successes
            if (pst.executeUpdate() > 0)
                result = true;
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
