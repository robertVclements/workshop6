package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TravelPackageDB {
    public static ObservableList<TravelPackage> getAcitveTravelPackages(){
        Connection conn = DBConnection.getDBConnection();
        ObservableList<TravelPackage> pkgs = FXCollections.observableArrayList();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from packages where PkgStatus = 'active'");
            while(rs.next()) {
                pkgs.add(new TravelPackage(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getDate(3).toLocalDate(),
                        rs.getDate(4).toLocalDate(),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getDouble(7),
                        rs.getString(8)));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pkgs;
    }

    public static TravelPackage getTravelPackageById(int pkgId){
        Connection conn = DBConnection.getDBConnection();
        TravelPackage travelPkg = null;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from packages where PackageId=" + pkgId);
            while(rs.next()) {
                travelPkg = new TravelPackage(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getDate(3).toLocalDate(),
                        rs.getDate(4).toLocalDate(),
                        rs.getString(5),
                        rs.getDouble(6),
                        rs.getDouble(7),
                        rs.getString(8));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return travelPkg;
    }
}
