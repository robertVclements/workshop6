package sample;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
public class ProductSupplier {

    private int productSupplierId;
    private Product product;
    private Supplier supplier;

    public ProductSupplier(int productSupplierId, Product product, Supplier supplier) {
        this.productSupplierId = productSupplierId;
        this.product = product;
        this.supplier = supplier;
    }

    public int getProductSupplierId() {
        return productSupplierId;
    }

    public void setProductSupplierId(int productSupplierId) {
        this.productSupplierId = productSupplierId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    @Override
    public String toString() {
        return product + ", " + supplier;
    }
}
