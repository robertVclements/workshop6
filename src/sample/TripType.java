package sample;

import javafx.beans.property.SimpleStringProperty;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */

public class TripType {

    private SimpleStringProperty tripTypeId;
    private SimpleStringProperty tTName;

    public TripType(String tripTypeId, String tTName){
        this.tripTypeId = new SimpleStringProperty(tripTypeId);
        this.tTName = new SimpleStringProperty(tTName);
    }

    public String getTripTypeId() { return tripTypeId.get();  }

    public void setTripTypeId(String tripTypeId) { this.tripTypeId.set(tripTypeId); }

    public String getTTName() { return tTName.get();  }

    public void setTTName(String tTName) { this.tTName.set(tTName); }

    @Override
    public String toString() {
        return tTName.get();
    }
}
