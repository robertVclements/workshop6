package sample;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    public static boolean isPositiveNumber(String value) {
        Pattern p = Pattern.compile("[1-9][0-9]*");
        Matcher m = p.matcher(value);
        if (m.find() && m.group().equals(value))
            return true;
        else
            return false;
    }

    public static boolean isCanadaPostal(String value) {
        Pattern p = Pattern.compile("[a-zA-Z][0-9][a-zA-Z]\\s[0-9][a-zA-Z][0-9]");
        Matcher m = p.matcher(value);
        if (m.find() && m.group().equals(value))
            return true;
        else
            return false;
    }

    public static boolean isEmail(String value) {
        Pattern p = Pattern.compile("^(.+)@(.+)$");
        Matcher m = p.matcher(value);
        if (m.find() && m.group().equals(value))
            return true;
        else
            return false;
    }

    public static boolean isPhoneNo(String value) {
        Pattern p = Pattern.compile("^([2-9]\\d{2})([2-9]\\d{2})(\\d{4})$");
        return Validation.match(p, value);
    }

    /*the requirement of password
        (?=.*[0-9]) a digit must occur at least once
        (?=.*[a-z]) a lower case letter must occur at least once
        (?=.*[A-Z]) an upper case letter must occur at least once
        (?=.*[@#$%^&+=]) a special character must occur at least once
        (?=\\S+$) no whitespace allowed in the entire string
         .{8,} at least 8 characters
     */
    public static boolean isPassword(String value) {

        Pattern p = Pattern.compile("(?=.*[0-9])(?=.*[A-Z])(?=\\\\S+$).{8,}");
        return Validation.match(p, value);
    }

    private static boolean match(Pattern p, String value) {
        Matcher m = p.matcher(value);
        if (m.find() && m.group().equals(value))
            return true;
        else
            return false;
    }


}
