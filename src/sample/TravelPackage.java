package sample;

import javafx.beans.property.SimpleObjectProperty;

import java.time.LocalDate;


public class TravelPackage {
    private int packageId;
    private String pkgName;
    private LocalDate pkgStartDate;
    private LocalDate pkgEndDate;
    private String pkgDesc;
    private double pkgBasePrice;
    private double pkgAgencyCommission;
    private String pkgStatus;


    public TravelPackage(int packageId, String pkgName, LocalDate pkgStartDate, LocalDate pkgEndDate, String pkgDesc,
                         double pkgBasePrice, double pkgAgencyCommission, String pkgStatus ) {
        this.packageId = packageId;
        this.pkgName = pkgName;
        this.pkgStartDate = pkgStartDate;
        this.pkgEndDate = pkgEndDate;
        this.pkgDesc = pkgDesc;
        this.pkgBasePrice = pkgBasePrice;
        this.pkgAgencyCommission = pkgAgencyCommission;
        this.pkgStatus = pkgStatus;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int agentID) {
        this.packageId = agentID;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public LocalDate getPkgStartDate() {
        return pkgStartDate;
    }

    public void setPkgStartDate(LocalDate pkgStartDate) {
        this.pkgStartDate = pkgStartDate;
    }

    public LocalDate getPkgEndDate() {
        return pkgEndDate;
    }

    public void setPkgEndDate(LocalDate pkgEndDate) {
        this.pkgEndDate = pkgEndDate;
    }

    public String getPkgDesc() {
        return pkgDesc;
    }

    public void setPkgDesc(String pkgDesc) {
        this.pkgDesc = pkgDesc;
    }

    public double getPkgBasePrice() {
        return pkgBasePrice;
    }

    public void setPkgBasePrice(double pkgBasePrice) {
        this.pkgBasePrice = pkgBasePrice;
    }

    public double getPkgAgencyCommission() {
        return pkgAgencyCommission;
    }

    public void setPkgAgencyCommission(double pkgAgencyCommission) {
        this.pkgAgencyCommission = pkgAgencyCommission;
    }

    public String getPkgStatus() {
        return pkgStatus;
    }

    public void setPkgStatus(String pkgStatus) {
        this.pkgStatus = pkgStatus;
    }




    //Lindsay's code
    @Override
    public String toString() {
        return pkgName;
    }
}
