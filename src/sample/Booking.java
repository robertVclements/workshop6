package sample;

import javafx.beans.property.*;

import java.time.LocalDate;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
public class Booking {

    private SimpleIntegerProperty bookingId;
    private SimpleObjectProperty<LocalDate> bookingDate;
    private SimpleStringProperty bookingNo;
    private SimpleIntegerProperty travelerCount;
    private SimpleIntegerProperty customerId;
    private SimpleObjectProperty<TripType> tripType;
    private SimpleObjectProperty<TravelPackage> travelPackage;
    private SimpleStringProperty bookingStatus;

    public Booking(int bookingId, LocalDate bookingDate, String bookingNo, Integer travelerCount,
                   int customerId, TripType tripType, TravelPackage travelPackage, String bookingStatus) {
        this.bookingId = new SimpleIntegerProperty(bookingId);
        this.bookingDate = new SimpleObjectProperty<LocalDate>(bookingDate);
        this.bookingNo = new SimpleStringProperty(bookingNo);
        this.travelerCount = new SimpleIntegerProperty(travelerCount);
        this.customerId = new SimpleIntegerProperty(customerId);
        this.tripType= new SimpleObjectProperty<TripType>(tripType);
        this.travelPackage = new SimpleObjectProperty<TravelPackage>(travelPackage);
        this.bookingStatus = new SimpleStringProperty(bookingStatus);

    }

    public int getBookingId() {
        return bookingId.get();
    }

    public void setBookingId(int bookingId) {
        this.bookingId.set(bookingId);
    }

    public LocalDate getBookingDate() {
        return bookingDate.get();
    }

    public void setBookingDate(LocalDate bookingDate) {
        this.bookingDate.set(bookingDate);
    }

    public String getBookingNo() { return bookingNo.get();  }

    public void setBookingNo(String bookingNo) {
        this.bookingNo.set(bookingNo);
    }

    public int getTravelerCount() {
        return travelerCount.get();
    }

    public void setTravelerCount(int travelerCount) {
        this.travelerCount.set(travelerCount);
    }

    public int getCustomerId() {
        return customerId.get();
    }

    public void setCustomerId(int customerId) {
        this.customerId.set(customerId);
    }

    public TripType getTripType() {
        return tripType.get();
    }

    public void setTripType(TripType tripType) {
        this.tripType.set(tripType);
    }

    public TravelPackage getTravelPackage() {
        return travelPackage.get();
    }

    public void setTravelPackage(TravelPackage travelPackage) {
        this.travelPackage.set(travelPackage);
    }

    public String getBookingStatus() {
        return bookingStatus.get();
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus.set(bookingStatus);
    }

}
