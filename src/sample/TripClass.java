package sample;

/* Author: Lindsay
   Date: October, 2018
   Purpose: student project
 */
public class TripClass {

    private String classId;
    private String className;
    private String classDesc;

    public TripClass(String classId, String className, String classDesc) {
        this.classId = classId;
        this.className = className;
        this.classDesc = classDesc;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassDesc() {
        return classDesc;
    }

    public void setClassDesc(String classDesc) {
        this.classDesc = classDesc;
    }

    @Override
    public String toString() {
        return className;
    }
}
