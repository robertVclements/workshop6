package sample;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProductSupplierDB {

    public static ObservableList<ProductSupplier> getAllProductSuppliers() {
       ObservableList<ProductSupplier> prodSupps = FXCollections.observableArrayList();
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from products_suppliers");
            while (rs.next()) {
                prodSupps.add(new ProductSupplier( rs.getInt(1),
                        ProductDB.getProductById(rs.getInt(2)),
                        SupplierDB.getSupplierById(rs.getInt(3))));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodSupps;
    }

    public static ProductSupplier getProductSupplierById(int prodSuppId) {
        ProductSupplier prodSupp = null;
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from products_suppliers where ProductSupplierId = " + prodSuppId);
            while (rs.next()) {
                prodSupp = new ProductSupplier( rs.getInt(1),
                        ProductDB.getProductById(rs.getInt(2)),
                        SupplierDB.getSupplierById(rs.getInt(3)));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodSupp;
    }



}
