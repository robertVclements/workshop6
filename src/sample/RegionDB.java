package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RegionDB {

    public static ObservableList<Region> getAllRegions() {
        ObservableList<Region> regions = FXCollections.observableArrayList();
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from regions");
            while (rs.next()) {
                regions.add(new Region( rs.getString(1),rs.getString(2)) );
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return regions;
    }


    public static Region getRegionById(String regionId) {
        Region region = null;
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from regions where regionId = '" + regionId + "'");
            while (rs.next()) {
                region = new Region( rs.getString(1),rs.getString(2) );
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return region;
    }
}
