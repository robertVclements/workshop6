package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FeeDB {

    public static ObservableList<Fee> getAllFee() {
        ObservableList<Fee> fees = FXCollections.observableArrayList();
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from fees");
            while (rs.next()) {
                fees.add(new Fee( rs.getString(1),
                        rs.getString(2),
                        rs.getBigDecimal(3),
                        rs.getString(4)));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fees;
    }

    public static Fee getFeeById(String regionId) {
        Fee fee = null;
        Connection conn = DBConnection.getDBConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from fees where feeId = '" + regionId + "'");
            while (rs.next()) {
                fee = new Fee( rs.getString(1),
                        rs.getString(2),
                        rs.getBigDecimal(3),
                        rs.getString(4));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fee;
    }
}
